from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, ItemForm

# Create your views here.


def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {
        'todo_list': todo_list,
    }
    return render(request, 'todos/list.html', context)


def todo_list_detail(request, id):
    list = TodoList.objects.get(id=id)
    context = {
        'list': list,
    }
    return render(request, "todos/detail.html", context)


def todo_list_update(request, id):
    list_details = get_object_or_404(TodoList, id=id)
    if request.method == 'POST':
        form = TodoForm(request.POST, instance=list_details)
        if form.is_valid():
            list = form.save()
            return redirect('todo_list_detail', list.id)
    else:
        form = TodoForm(instance=list_details)
    context = {
        'edit_form': list_details,
        'form': form,
    }
    return render(request, 'todos/edit.html', context)


def todo_list_delete(request, id):
    if request.method == "POST":
        todo_list =TodoList.objects.get(id=id)
        todo_list.delete()
        return redirect('todo_list_list')
    return render(request, 'todos/delete.html')


def todo_item_create(request):
    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid():
            todo_item = form.save()
            return redirect('todo_list_detail', todo_item.list.id)
    else:
        form = ItemForm
    context = {
        'form': form,
    }
    return render(request, 'todos/create_item.html', context)


def create_todo_list(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect('todo_list_detail', id=list.id)
    else:
        form = TodoForm
    context = {
        'form': form,
    }
    return render(request, 'todos/create.html', context)


def todo_item_update(request, id):
    item_details = TodoItem.objects.get(id=id)
    if request.method == 'POST':
        form = ItemForm(request.POST, instance=item_details)
        if form.is_valid():
            item = form.save()
            return redirect('todo_list_detail', id=item.list.id)
    else: 
        form = ItemForm(instance=item_details)
    context = {
        'form': form,
    }
    return render(request, 'todos/edit_item.html', context)